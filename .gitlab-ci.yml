# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

# Common parameters
# ===========================================================================
.changes_docker: &changes_docker
  changes:
    - ".dockerignore"
    - "Dockerfile"
    - "bin/*.sh"
    - "dockerfiles/**/*"

.extra_variables: &extra_variables # Workaround for gitlab-org/gitlab#196452
  - export TARGET_ARCH="${CI_JOB_NAME##*_}"

default:
  before_script:
    - *extra_variables
  tags:
    - dntd
    - docker

stages:
  - lint
  - build
  - test
  - branch
  - update
  - release
  - deploy
  - complete


# Linting
# ===========================================================================
.linting:
  stage: lint
  only:
    refs:
      - branches
      - merge_requests
      - tags

shellscript_linting:
  extends: .linting
  image: "registry.hub.docker.com/koalaman/shellcheck-alpine:stable"
  before_script:
    - shellcheck --version
  script:
    - shellcheck --color=always --format=tty --shell=sh --external-sources **/*.sh .githooks/*

dockerfile_linting:
  extends: .linting
  image: "registry.hub.docker.com/hadolint/hadolint:latest-debian"
  before_script:
    - hadolint --version
  script:
    - hadolint --format tty **Dockerfile*


# Build
# ===========================================================================
.docker:
  only:
    <<: *changes_docker
    refs:
      - branches
      - merge_requests
      - tags
  image: "registry.hub.docker.com/library/docker:stable"

.build:
  extends: .docker
  before_script:
    - *extra_variables
  stage: build
  script:
    - |
      docker build \
             --build-arg "TARGET_ARCH=${TARGET_ARCH}" \
             --pull \
             --rm \
             --tag "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" \
             .

.test_image:
  extends: .docker
  stage: test
  script:
    - "/test/buildenv_check.sh"


# Release branches
# ===========================================================================
.git:
  image: "registry.hub.docker.com/gitscm/git:latest"
  before_script:
    - *extra_variables
    - git config --local user.name "${GITLAB_USER_NAME}"
    - git config --local user.email "${GITLAB_USER_EMAIL}"
    - git config --local credential.helper "cache --timeout=2147483647"
    - printf "url=${CI_PROJECT_URL}\nusername=${CI_BOT_USER}\npassword=${CI_PERSONAL_TOKEN}\n\n" | git credential approve
    - git remote set-url --push origin "https://${CI_BOT_USER}@${CI_REPOSITORY_URL#*@}"
  after_script:
    - git credential-cache exit

create_release_branch:
  extends: .git
  stage: branch
  only:
    refs:
      - /^v\d+\.\d+$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab#27863
      - merge_requests
  script:
    - git_tag="$(git describe --exact-match --match "v[0-9]*")"
    - git_tag_msg="$(git tag --format="%(contents:subject)%0a%0a%(contents:body)%0a%0a(Auto-created release candidate)" --list "${git_tag}")"
    - git checkout "${CI_COMMIT_SHA}" -b "release/${CI_COMMIT_TAG:?}"
    - git tag --annotate --message="${git_tag_msg:-See tag '${CI_COMMIT_TAG}'}" "${CI_COMMIT_TAG}.0-rc1"
    - git push --follow-tags origin "HEAD"


# Deploy containers
# ===========================================================================
.deploy:
  extends: .docker
  stage: deploy
  before_script:
    - *extra_variables
    - echo "${CI_JOB_TOKEN}" | docker login --username "gitlab-ci-token" --password-stdin "${CI_REGISTRY}"

.deploy_builds:
  extends: .deploy
  only:
    <<: *changes_docker
    refs:
      - /^release/v\d+\.\d+.*$/
  except:
    refs:
      - merge_requests
      - tags # Workaround for gitlab-org/gitlab#27863
  script:
    - docker tag  "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"
    - docker push "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"

.deploy_release:
  extends: .deploy
  only:
    <<: *changes_docker
    refs:
      - /^(.*/)?v\d+(\.\d+){2,}(-rc\d+)?$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab#27863
      - merge_requests
  script:
    - docker_tag="${CI_COMMIT_TAG##*/}"
    - docker tag  "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:${docker_tag:?}"
    - docker push "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:${docker_tag:?}"
    - docker tag  "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:${docker_tag%.*}"
    - docker push "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:${docker_tag%.*}"
    - |
      latest_major_minor="$(wget -q -O - \
                                 --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" \
                                 "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories?tags=true"| \
                    sed \
                        -e 's|[][]||g' \
                        -e 's|[[:space:]]||g' \
                        -e 's|"||g' \
                        -e 's|}|\n|g' \
                        -e 's|,{|{|g' | \
                    sed -n 's|.*location:'"${CI_REGISTRY_IMAGE}/${CI_PROJECT_NAME}"':v\([[:digit:]]\+\.[[:digit:]]\+\).*|\1|p' | \
                    sort -n -r -t '.' | \
                    uniq | \
                    head -n 1)"
      latest_major="${latest_major_minor%%.*}"
      docker_tag_major="${docker_tag%%.*}"
      if [ "${docker_tag_major#v*}" -lt "${latest_major?}" ]; then
        return
      fi
      latest_minor="${latest_major_minor#*.}"
      docker_tag_minor_patch="${docker_tag#${docker_tag_major}.}"
      docker_tag_minor="${docker_tag_minor_patch%.*}"
      docker_tag_patch="${docker_tag_minor_patch#*.}"
      if [ "${docker_tag_major#v*}" -eq "${latest_major?}" ] && \
         [ "${docker_tag_minor}" -lt "${latest_minor?}" ]; then
          return
      fi
      echo "Pushing ${docker_tag} as latest"
      docker tag  "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:latest"
      docker push "${CI_REGISTRY_IMAGE}/${TARGET_ARCH}/${CI_PROJECT_NAME}:latest"


# Complete
# ===========================================================================
announce_release:
  image: "registry.hub.docker.com/olliver/curl:latest" # Workaround until this is merged upstream
  stage: complete
  when: delayed
  start_in: 86 minutes
  before_script:
    - *extra_variables
    - apk add --no-cache git # Workaround for gitlab-org/gitlab-ce#59608
  only:
    <<: *changes_docker
    refs:
      - /^v\d+\.\d+(\.\d+)+$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab-ce#27818
      - merge_requests
  script:
    - CI_COMMIT_TAG_MESSAGE="$(git tag --format="%(contents:subject)%0a%0a%(contents:body)" --list ${CI_COMMIT_TAG:?})" # Workaround for gitlab-org/gitlab-ce#59608
    - | # Workaround for gitlab-org/gitlab-ce#59726
      CI_COMMIT_TAG_MESSAGE="$(echo "${CI_COMMIT_TAG_MESSAGE}" | \
                               tr -d '\b' | \
                               tr '\f' '\n' | \
                               sed \
                                   -e 's|\\|\\\\|g' \
                                   -e 's|"|\\"|g' \
                                   -e 's|/|\\/|g' \
                                   -e ':a;N;$!ba;s|\n|\\n|g' \
                                   -e 's|\r|\\r|g' \
                                   -e 's|\t|\\t|g')"
    - |
      curl \
        --data "{\"tag_name\": \"${CI_COMMIT_TAG}\", \"name\": \"${CI_PROJECT_NAME} ${CI_COMMIT_TAG}\", \"description\": \"${CI_COMMIT_TAG_MESSAGE:-No release notes.}\"}" \
        --fail \
        --header "Content-Type: application/json" \
        --header "Private-Token: ${CI_PERSONAL_TOKEN}" \
        --output "/dev/null" \
        --request POST \
        --show-error \
        --silent \
        --write-out "HTTP response: %{http_code}\n\n" \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"

.cleanup_docker_containers:
  extends: .docker
  stage: complete
  when: always
  script:
    - |
      if docker inspect --type image "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" 1> "/dev/null"; then
        docker rmi "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
      fi


# Multi-arch jobs
# Arch: AMD64/x86_64
# ===========================================================================
build_amd64:
  extends: .build

test_image_amd64:
  extends: .test_image
  image: "amd64/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  needs:
    - job: build_amd64
      artifacts: false

deploy_builds_amd64:
  extends: .deploy_builds

deploy_release_amd64:
  extends: .deploy_release

cleanup_docker_containers_amd64:
  extends: .cleanup_docker_containers

# Arch: arm32v7/armv7
# ===========================================================================
build_arm32v7:
  extends: .build

test_image_arm32v7:
  extends: .test_image
  image: "arm32v7/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  needs:
    - job: build_arm32v7
      artifacts: false

deploy_builds_arm32v7:
  extends: .deploy_builds

deploy_release_arm32v7:
  extends: .deploy_release

cleanup_docker_containers_arm32v7:
  extends: .cleanup_docker_containers

# Arch: arm64v8/aarch
# ===========================================================================
build_arm64v8:
  extends: .build

test_image_arm64v8:
  extends: .test_image
  image: "arm64v8/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  needs:
    - job: build_arm64v8
      artifacts: false

deploy_builds_arm64v8:
  extends: .deploy_builds

deploy_release_arm64v8:
  extends: .deploy_release

cleanup_docker_containers_arm64v8:
  extends: .cleanup_docker_containers

# Arch: arm32v6/armhf
# ===========================================================================
build_arm32v6:
  extends: .build

test_image_arm32v6:
  extends: .test_image
  image: "arm32v6/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  needs:
    - job: build_arm32v6
      artifacts: false

deploy_builds_arm32v6:
  extends: .deploy_builds

deploy_release_arm32v6:
  extends: .deploy_release

cleanup_docker_containers_arm32v6:
  extends: .cleanup_docker_containers

# Arch: i386/x86
# ===========================================================================
build_i386:
  extends: .build

test_image_i386:
  extends: .test_image
  image: "i386/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  needs:
    - job: build_i386
      artifacts: false

deploy_builds_i386:
  extends: .deploy_builds

deploy_release_i386:
  extends: .deploy_release

cleanup_docker_containers_i386:
  extends: .cleanup_docker_containers
